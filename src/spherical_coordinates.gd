extends Node

#################
# Main Algorithm
#################
func general_positions(I: Vector3, E: Vector3, max_units: int, unit_size: float, gap: float) -> Array:
	var radial_vector = E - I
	
	# Spherical Coordinates
	var spherical_coords = cartesian_to_spherical(I, E)
	var theta = spherical_coords[0]
	var phi = spherical_coords[1]
	var rho = spherical_coords[2]
	
	var dist_btxt = unit_size + gap
	var unit_on_radial = units_on_radius(rho, dist_btxt)
	
	var total_depth = total_depths(max_units, unit_on_radial)
	
	# Visual help
	var debug_draw = $"/root/DebugOverlay/DebugDraw3D"
	debug_draw.add_vector(I, I + radial_vector, Color(0,0,1))
	
	var current_I = I
	var ps = []
	
	for curr_depth in range(0,total_depth+1):
		var current_row = curr_depth * unit_on_radial
		for curr_pos in unit_on_radial:
			var current_unit = current_row + curr_pos 
			if current_unit < max_units:
				var position = positions_on_radial_vector(current_I, theta, phi, dist_btxt, curr_pos)
				ps.append(position)
			else:
				print_debug("Exhausted all units")
				return ps
				
		current_I = depth_positions(current_I, phi, theta, dist_btxt, rho)
	
	return ps

########################
# Equations/Functions
########################

func cartesian_to_spherical(I: Vector3, E: Vector3) -> Array:
	var vector = E - I
	var rho = vector.length()
	var theta = acos(vector.y/rho)
	var phi = atan2(vector.z, vector.x)
	
	var coordinates = [theta, phi, rho]
	return coordinates
	
func spherical_to_cartesian(theta: float, phi: float, rho) -> Vector3:
	var z = rho * sin(theta) * sin(phi)
	var y = rho * cos(theta)
	var x = rho * sin(theta) * cos(phi)
	
	return Vector3(x,y,z)
	
func positions_on_radial_vector(I: Vector3, theta: float, phi: float, dist_btxt: float, index: int):
	var v = spherical_to_cartesian(theta, phi, dist_btxt * index)
	
	return I + v

func depth_positions(I: Vector3, phi: float, theta: float, dist_btxt: float, rho: float) -> Vector3:
	
	var offset_side = -sin(phi) * dist_btxt 	# Accommodate for godot's coordinate system
	var offset_forward = cos(phi) * dist_btxt 
	var direction_behind = Vector3(offset_side, 0, offset_forward)
	
	return I + direction_behind

func units_on_radius(rho: float, dst_btxt: float) -> int:
	var units = rho/dst_btxt
	units = round(units) + 1	# Have one unit on the clicked spot
	return units
	
func total_depths(max_units: int, units_on_radial_vector: int) -> int:
	var full_row = max_units / units_on_radial_vector

	var depths = full_row - 1
	
	# include incomplete row
	if (max_units % units_on_radial_vector > 0):
		depths += 1
	
	return depths
