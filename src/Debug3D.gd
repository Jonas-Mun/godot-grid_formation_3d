extends Control

var camera: Camera
var entity

var width = 1

var I: Vector3 = Vector3.ZERO
var E: Vector3 = Vector3.ZERO

var vectors = []

func _draw():
	var color = Color(0, 1, 0)
	#var start = camera.unproject_position(entity.global_transform.origin)
	var start = camera.unproject_position(I)
	#var end = camera.unproject_position(entity.global_transform.origin + entity.velocity)
	var end = camera.unproject_position(E)
	self.draw_line(start, end, color, width)
	self.draw_triangle(end, start.direction_to(end), width*2, color)
	
	for v in vectors:
		v.draw(self, camera)

func draw_triangle(pos, dir, size, color):
	var a = pos + dir * size
	var b = pos + dir.rotated(2*PI/3) * size
	var c = pos + dir.rotated(4*PI/3) * size
	var points = PoolVector2Array([a, b, c])
	draw_polygon(points, PoolColorArray([color]))

func set_line(i, e):
	I = i
	E = e
	
class Vector:
	var start_point
	var end_point
	var color
	
	func _init(i, e,c):
		start_point = i
		end_point = e
		color = c
		
	func draw(node, camera):
		var start = camera.unproject_position(start_point)
		var end = camera.unproject_position(end_point)
		node.draw_line(start, end, color, 1)
		node.draw_triangle(end, start.direction_to(end), 1*2, color)

func _process(delta):
	update()

func set_camera(cam):
	camera = cam
	
func add_vector(p0, p1,c):
	var v = Vector.new(p0, p1,c)
	vectors.append(v)
