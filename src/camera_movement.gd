extends Spatial

var turn_speed = 35
var speed = 10

var rot_x = 0.0
var rot_y = 0.0
var rot_z = 0.0

var camrot_h = 0
var camrot_v = 0
var cam_v_max = 75
var cam_v_min = -55
var h_sensitivity = 0.1
var v_sensitivity = 0.1
var h_acceleration = 10
var v_acceleration = 10

var shift_pressed: bool = false
var I
var E

var DRAW_TERRAIN = true

signal debug_line(i,e)
signal draw_terrain_points(i,e)

export (String, FILE, "*.tscn") var sphere_scene
export (int) var max_units = 10
export (float) var unit_size = 1
export (float) var gap = 1

func _ready():
	var debug_overlay = $"/root/DebugOverlay/DebugDraw3D"
	debug_overlay.set_camera(get_camera())
	connect("debug_line",debug_overlay, "set_line")

func _input(event):
	if event is InputEventMouseMotion and shift_pressed:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		camrot_h += -event.relative.x * h_sensitivity
		camrot_v += -event.relative.y * v_sensitivity
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
	if event.is_action_pressed("shift"):
		shift_pressed = true
	if event.is_action_released("shift"):
		shift_pressed = false

func _physics_process(delta):
	get_input(delta)
	
	camrot_v = clamp(camrot_v, cam_v_min, cam_v_max)
	
	var a = Quat(transform.basis)
	
	
	transform.basis = Basis()
	rotate_object_local(Vector3(0,1,0), camrot_h * delta)
	rotate_object_local(Vector3(1,0,0), camrot_v * delta)
	var b = transform.basis
	var c = a.slerp(b,0.5)
	transform.basis = Basis(c)
	
	#rotation_degrees.y = lerp(rotation_degrees.y, camrot_h, delta * h_acceleration)
	#rotation_degrees.x = lerp(rotation_degrees.x, camrot_v, delta * v_acceleration)
	
	#rotation_degrees.x = rot_x
	#rotation_degrees.y = rot_y
	#rotation_degrees.z = rot_z
	#translate(-$CameraRig.transform.basis.z * speed * delta)
	
func get_input(delta: float):
	if Input.is_action_pressed("forward"):
		translate(-$CameraRig.transform.basis.z * speed * delta)
	if Input.is_action_pressed("backward"):
		translate($CameraRig.transform.basis.z * speed * delta)
	if Input.is_action_pressed("left"):
		translate(-$CameraRig/Camera.transform.basis.x * speed * delta)
	if Input.is_action_pressed("right"):
		translate($CameraRig/Camera.transform.basis.x * speed * delta)
	if Input.is_action_just_pressed("left_click"):
		I = get_clicked_position(get_world(), get_viewport(), get_camera())
	if Input.is_action_just_released("left_click"):
		E = get_clicked_position(get_world(), get_viewport(), get_camera())
		var positions = SphericalCoordinates.general_positions(I, E, max_units, unit_size, gap)
		for p in positions:
			spawn_sphere(p)
		
func get_camera() -> Camera:
	var cam = $CameraRig/Camera
	return cam

func get_clicked_position(world: World, viewport: Viewport, camera: Camera):
	var space_state = world.direct_space_state	# Physics
	var mouse_position = viewport.get_mouse_position()
	var ray_origin = camera.project_ray_origin(mouse_position)
	var ray_end = ray_origin + camera.project_ray_normal(mouse_position) * 200

	var intersection = space_state.intersect_ray(ray_origin, ray_end)
	
	if intersection.empty() == false:
		return intersection.position
	else:
		return null

# Debug purposes
func spawn_sphere(p: Vector3):
	var sphere = load(sphere_scene).instance()
	sphere.transform.origin = p
	get_parent().add_child(sphere)
