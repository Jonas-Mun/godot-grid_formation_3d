extends Node


var key_maps = {
	"shift": KEY_SHIFT,
	"forward": KEY_W,
	"backward": KEY_S,
	"left": KEY_A,
	"right": KEY_D,
}

var mouse_keys = {
	"camera_rotate": BUTTON_MIDDLE,
	"camera_zoom_in": BUTTON_WHEEL_UP,
	"camera_zoom_out": BUTTON_WHEEL_DOWN,
	"camera_pan": BUTTON_RIGHT,
	"left_click": BUTTON_LEFT,
}

func _ready():
	add_inputs(key_maps)
	add_inputs(mouse_keys)
	
	map_mouse_input(mouse_keys)
	map_key_inputs(key_maps)

func add_inputs(inputs: Dictionary):
	for k in inputs.keys():
		InputMap.add_action(k)

func map_mouse_input(mouse_input: Dictionary):
	for k in mouse_input:
		InputMap.action_erase_events(k)
		var ev = InputEventMouseButton.new()
		ev.button_index = mouse_input[k]
		InputMap.action_add_event(k, ev)
		
func map_key_inputs(key_inputs: Dictionary):
	for k in key_inputs:
		InputMap.action_erase_events(k)
		var ev = InputEventKey.new()
		ev.scancode = key_inputs[k]
		InputMap.action_add_event(k, ev)
